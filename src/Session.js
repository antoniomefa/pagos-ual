import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import { withStyles } from '@material-ui/core/styles';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import URL_BASE from './constants';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

class Session extends React.Component {
    state = {
        openSnack: false,
        msgSnack: '',
        open: false,
        username: '',
        password: ''
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({
            open: false,
            username: '',
            password: '',
            openSnack: false,
            msgSnack: ''
        });
    };

    hasError = (field) => {
        if(this.props.formSubmitted) {
            if(this.props.state[field] === '') {
                return true;
            }
        }

        return false;
    };

    handleChange = (field, event) => {
        let value = event.target.value;
        let state = this.state;
        state[field] = value;
        this.setState(state);
    };

    login = () => {
        const { username, password } = this.state;

        if(username.length === 0) {
            this.setState({
                openSnack: true,
                msgSnack: 'El username es obligatorio'
            });
            return;
        }

        if(password.length === 0) {
            this.setState({
                openSnack: true,
                msgSnack: 'La contraseña es obligatoria'
            });
            return;
        }

        fetch(`${URL_BASE}api/Authenticate/login?username=${username}&password=${btoa(password)}&id_colegio=1`)
            .then(res => {
                if(res.status === 200) {
                    localStorage.setItem('tkn', res.headers.get('token'));

                }
                return res.json();
            })
            .then(json => {
                if(!localStorage.getItem('tkn')) {
                    throw json;
                }
                localStorage.setItem('usr', JSON.stringify(json));
                this.handleClose();
                this.props.change();
            })
            .catch(err => {
                this.onOpenSanck(err);
            });
    }

    loguedIn = () => {
        return !!localStorage.getItem('usr');
    }

    user = () => {
        let ls = JSON.parse(localStorage.getItem('usr'));
        return ls;
    }

    logout = () => {
        localStorage.clear();
        this.setState({
            open: false
        }, this.props.change);
    }

    onOpenSanck = (msg) => {
        this.setState({
            openSnack: true,
            msgSnack: msg
        });
    }

    onCloseSnack = () => {
        this.setState({ openSnack: false });
    }

    render() {
        const { classes } = this.props;
        const { openSnack } = this.state;
        return (
            <div>
                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    open={openSnack}
                    onClose={this.onCloseSnack}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{this.state.msgSnack}</span>}
                />
                {
                    this.loguedIn() ? (
                        <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                            <h4 style={{ margin: 0, textAlign: 'center' }}>Bienvenido {this.user().Nombres} {this.user().Apellido_Paterno}</h4>
                            <small onClick={this.logout} className="__point">cerrar sesión</small>
                        </div>
                    ) : (
                        <span onClick={this.handleClickOpen} className="__point">Iniciar sesión</span>
                    )
                }
                <Dialog
                    open={this.state.open}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle id="alert-dialog-slide-title">
                        {"Inicia sesión"}
                    </DialogTitle>
                    <DialogContent>
                    <small>Recuerda que accedes con tus credenciales de la app.</small>
                        <Grid container className={classes.container}>
                            <Grid item container xs={12}>
                                <FormControl fullWidth className={classes.formControl} error={this.hasError('username')}>
                                    <InputLabel htmlFor="component-simple">Correo</InputLabel>
                                    <Input
                                        id="component-simple"
                                        value={this.state.username}
                                        onChange={this.handleChange.bind(this, 'username')}
                                        aria-describedby="component-helper-text"
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item container xs={12}>
                                <FormControl fullWidth className={classes.formControl} error={this.hasError('password')}>
                                    <InputLabel htmlFor="component-helper">Contraseña</InputLabel>
                                    <Input
                                        id="component-helper"
                                        type="password"
                                        value={this.state.password}
                                        onChange={this.handleChange.bind(this, 'password')}
                                        aria-describedby="component-helper-text"
                                    />
                                </FormControl>
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancelar
                        </Button>
                        <Button variant="contained" color="primary" onClick={this.login}>
                            Iniciar sesión
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default  withStyles(styles)(Session);
