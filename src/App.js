import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Form from './Form';
import Session from './Session';
import { SnackbarProvider } from 'notistack';

class App extends Component {
    loguedIn = () => {
        return !!localStorage.getItem('usr');
    }

    state = {
        loguedIn: this.loguedIn()
    };

    sessionChange = () => {
        this.setState({
            loguedIn: this.loguedIn()
        });
    }

    render() {
        return (
            <SnackbarProvider maxSnack={3}>
                <div className="App">
                    <header className="App-header">
                        <div className="App-header-logo">
                            <img src={logo} className="App-logo" alt="logo" />
                        </div>
                        <div className="App-header-info">
                            <h3>Sistema de pagos de la <strong>Universidad America Latina</strong></h3>
                            <Session change={this.sessionChange}/>
                        </div>
                    </header>
                    <Form loguedIn={this.state.loguedIn} />
                </div>
            </SnackbarProvider>
        );
    }
}

export default App;
