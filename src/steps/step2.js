import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import NumberFormat from 'react-number-format';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import ListItemText from '@material-ui/core/ListItemText';
import URL_BASE from '../constants';

const DEFTKN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJDYW1wdXNWaXJ0dWFsQXBpIiwiaWF0IjoiMTU1NTgwMjcwOCIsImV4cCI6IjE4NzE0MjE5MDUiLCJlbWFpbCI6IldlYlBheVBsdXNAc2lzLmVkdS5teCIsImlkX2luc3RpdHVjaW9uIjoiMSIsImlkX3BlcnNvbmEiOiIyIiwiY2FuYWxfY29tdW5pY2FjaW9uIjoiIiwibW9vZGxlX3VzZXJfaWQiOiIiLCJtb29kbGVfdG9rZW4iOiIiLCJyb2wiOiIiLCJhbHVtbm8iOiIiLCJnbXQiOiItNiJ9.fTo8irf2IeUcDR8tOjU81TVVAXgrg_yf9yYZjIM9jbk';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

class Step2 extends React.Component {
    
    constructor(...props) {
        super(...props)
        this.state = {
            cuenta: null,
            concepts: {},
            amount: null
        }
      }

    componentDidMount() {
        this.forceUpdate();
        this.getAccount();
        this.requestService('concepts', 'listaservicios', this.props.idcampus, this.props.idniveleducativo)
    }

    getAccount = () => {
        if(this.loguedIn()) {
            fetch(`${URL_BASE}api/estadocuenta?jsonParameters={"nombreSolicitud":"getestadocuenta"}`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('tkn')}`
                }
            })
            .then(res => res.json())
            .then(response => {
                this.setState({ cuenta: response }, () => {
                    this.props.setAccount(response);
                });
            });
        }
    }

    loguedIn = () => {
        return !!localStorage.getItem('usr');
    }

    requestService = (field, solicitud, idcampus, idNivEdu) => {
        var query = `"nombreSolicitud":"${solicitud}"`
        switch(field){
            case 'concepts': {
                query += `,"idcampus":${idcampus}, "idniveleducativo":${idNivEdu}`;
                break;
            }
        }
        fetch(`${URL_BASE}api/campus?jsonParameters={${query}}`,
        {
            method: 'get',
            headers: {
                'Authorization': `Bearer ${DEFTKN}`
            }
        })
        .then(response => { 
            response.json().then( res => {
            this.setState({ [field]: res })
            })
        })
    }

    handleChange = async (field, event) => {
        let value = event.target.value;
        await this.props.changeState(field, value);
        if(field === 'concepto') {
            setTimeout(() => {
                if(this.loguedIn() && this.state.cuenta != null) {
                    let amount = this.state.cuenta.Cuentas[value].balance;
                    this.props.changeState('cantidad', amount);
                } else {
                    var amount = 0
                    for(let i=0; i<=this.state.concepts.length-1; i++){   
                        if (this.state.concepts[i].Nombre_Servicio == this.props.state.concepto){
                            amount = this.state.concepts[i].Costo;
                        }
                    }
                    this.props.changeState('cantidad', amount);
                }
            }, 200);
        }
    };

    hasError = (field) => {
        if(this.props.formSubmitted) {
            if(this.props.state[field] === '') {
                return true;
            }
        }
        return false;
    };

    getConceptos = () => {
        if(this.loguedIn() && this.state.cuenta != null) {
            return this.state.cuenta.Cuentas.map((d, i) => (
                <MenuItem key={i} value={i}>
                    {d.Nombre_Cuenta}
                </MenuItem>
            ))
        }

        if(!this.loguedIn()) {
            return Object.keys(this.state.concepts).map(index => (
                <MenuItem key={index} value={this.state.concepts[index].Nombre_Servicio}>
                    {this.state.concepts[index].Nombre_Servicio}
                </MenuItem>
            ))
        }

    }

    generate = () => {
        return this.state.cuenta.Cuentas[this.props.state.concepto].lista_cargos.map((value, index) =>
            <ListItem key={index}>
                <ListItemText
                    primary={value.concepto}
                />
            </ListItem>
        );
    }

    render() {
        const { classes } = this.props;
        return (
            <Grid container className={classes.container}>
                <Grid item container xs={12} lg={6}>
                    <TextField
                        id="standard-select-currency"
                        select
                        error={this.hasError('concepto')}
                        fullWidth
                        label="Concepto"
                        className={classes.textField+' '+classes.formControl}
                        value={this.props.state.concepto}
                        onChange={this.handleChange.bind(this, 'concepto')}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        helperText={this.hasError('concepto') ? "Este campo es requerido" : "Selecciona un concepto"}
                        margin="normal"
                    >
                        {this.getConceptos()}
                    </TextField>
                </Grid>
                <Grid item container xs={12} lg={6}>
                    <TextField
                        className={classes.formControl}
                        label="Cantidad a pagar"
                        fullWidth
                        error={this.hasError('cantidad')}
                        helperText={this.hasError('apellidos') ? "Este campo es requerido" : "Cantidad a pagar"}
                        value={this.props.state.cantidad}
                        onChange={this.handleChange.bind(this, 'cantidad')}
                        id="formatted-numberformat-input"
                        InputProps={{
                          inputComponent: NumberFormatCustom,
                        }}
                    />
                </Grid>
                <Grid item container xs={12} lg={6}>
                    {
                        this.loguedIn() && !!this.state.cuenta && this.props.state.concepto !== '' && (
                            <div>
                                <Typography variant="h6" className={classes.title}>
                                    Conceptos
                                </Typography>
                                <div className={classes.demo}>
                                    <List dense={false}>
                                        {this.generate()}
                                    </List>
                                </div>
                            </div>
                        )
                    }
                </Grid>
            </Grid>
        );
    }
}

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      prefix="$"
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

Step2.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Step2);
