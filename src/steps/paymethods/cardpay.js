import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withSnackbar } from 'notistack';
import CircularProgress from '@material-ui/core/CircularProgress';
import URL_BASE from '../../constants';

const DEFTKN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJDYW1wdXNWaXJ0dWFsQXBpIiwiaWF0IjoiMTU1NTgwMjcwOCIsImV4cCI6IjE4NzE0MjE5MDUiLCJlbWFpbCI6IldlYlBheVBsdXNAc2lzLmVkdS5teCIsImlkX2luc3RpdHVjaW9uIjoiMSIsImlkX3BlcnNvbmEiOiIyIiwiY2FuYWxfY29tdW5pY2FjaW9uIjoiIiwibW9vZGxlX3VzZXJfaWQiOiIiLCJtb29kbGVfdG9rZW4iOiIiLCJyb2wiOiIiLCJhbHVtbm8iOiIiLCJnbXQiOiItNiJ9.fTo8irf2IeUcDR8tOjU81TVVAXgrg_yf9yYZjIM9jbk'

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    },
});

class CardPay extends Component {

    constructor(...props) {
        super(...props)
        this.state = {
          scr: null
        }
      }

    loguedIn = function() {
        return !!localStorage.getItem('usr');
    };

    getIdCuenta = function() {
        if(this.loguedIn() && this.props.account) {
            return this.props.account.Cuentas[this.props.step2.concepto].id_Cuenta;
        }
        return -1; // Cuando no está logueado
    };

    getConcepto = function() {
        if(this.loguedIn() && this.props.account) {
            return this.props.account.Cuentas[this.props.step2.concepto].Nombre_Cuenta;
        }
        return this.props.step2.concepto;
    };

    getToken = function() {
        if(this.loguedIn() && this.props.account) {
            return localStorage.getItem('tkn') 
        }
        return DEFTKN;
    };

    getLabels = function() {
        if(this.props.account) {
            return [];
        }
        return [
            {
                Etiqueta: "nombre",
                Valor: this.props.step1.nombres + ' ' + this.props.step1.apellidos,
            },
            {
                Etiqueta: 'correo',
                Valor: this.props.step1.correo,
            },
            {
                Etiqueta: 'telefono',
                Valor: this.props.step1.telefono,
            },
            {
                Etiqueta: 'college',
                Valor: this.props.step1.nivel + ' - ' + this.props.step1.carrera + ' - ' + this.props.step1.modalidad + ' - ' + this.props.step1.campus,
            }
        ];
    }

    callPayModule() {
        if(this.state.src != null) {
            return;
        }
        debugger
        var data = {
            id_persona: this.props.account ? this.props.account.id_Persona : -1,
            amount: this.props.step2.cantidad,
            etiquetas: [
                ...this.getLabels(),
                {
                    Etiqueta: "id_Cuenta",
                    Valor: this.getIdCuenta(),
                },
                {
                    Etiqueta: "concepto",
                    Valor: this.getConcepto(),
                }
            ]
        };
        fetch(`${URL_BASE}api/WebPayPlus`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.getToken()}`,
            },
            body: JSON.stringify(data)
        })
        .then( (response) => {
            if( response.status !== 200) {
                this.props.enqueueSnackbar("No se puede realizar el pago en este momento, intentalo más tarde.",
                 { variant: 'warning', autoHideDuration: 10000, });
              }
            response.json().then(res => {
                if(typeof res == "string"){
                    this.props.enqueueSnackbar(res, { variant: 'error', autoHideDuration: 10000, });
                    throw res;
                  } else {
                    this.setState({ src: res.data.nb_url })
                  }
            });
        })
        .catch(err => {
            if (typeof err === 'object') {
                const message500 = 'Hubo un error en el servidor, intentalo más tarde.';
                this.props.enqueueSnackbar(message500, { variant: 'warning' });
            } else if (typeof err === 'string') {
              this.props.enqueueSnackbar(err, { variant: 'warning' });
            }
            throw err;
          });
    }

    componentDidMount(){
        this.callPayModule()
    }
    
    render() {
        const { classes } = this.props;
        return (    
            <div className={this.state.src != null ? "resp-container" : ""}>
                {
                    this.state.src == null ? (
                        <CircularProgress className={classes.progress}/>
                    ) : (
                        <iframe
                            className="resp-iframe"
                            src={this.state.src}
                            gesture="media"
                            allow="encrypted-media"
                            title={'MIT'}
                            width="600"
                            height="700"
                            frameBorder="0"
                            allowFullScreen
                            >
                        </iframe>
                    )
                }
            </div>
        );

    }
    
}

export default withStyles(styles)(withSnackbar(CardPay))

